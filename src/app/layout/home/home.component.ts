import { Component, OnInit, resolveForwardRef, OnDestroy } from "@angular/core";
import { ApiService } from "src/app/shared/services/api.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy {
  items: any = [];
  produtos: any = [];
  shops: any = [];
  search = '';
  constructor(private api: ApiService, private router: Router) {}

  ngOnInit() {
    this.getSector(11);
    this.getProducts(11);
  }

  getSector(codigo: number) {
    this.api.getSetor().subscribe(datas => {
      const filter = datas.filter(rows => {
        return rows.dep_id === codigo;
      });
      this.items = filter;
    });
  }

  ngOnDestroy() {
    this.shops = [];
  }

  onSector(event) {
    this.shops = [];
    if (!event.target.value || event.target.value === 'All') {
      this.getProducts(11);
    } else {
      this.getProducts2(event.target.value);
    }
  }

  onRegiao(event) {
    if (event.target.value === 5) {
      this.router.navigate([""]);
    } else {
      this.router.navigate(["santa-catarina"]);
    }
  }

  onSearch(e: any) {
    this.shops = [];
    this.api.getProductDepartamentoSearch(e.target.value).subscribe(data => {
      data.forEach(rows => {
        const region = rows.data.lst_preco_regiao;
        region.forEach(el => {
          if (el.cod_regiao === 5) {
            this.shops.push({
              dsc_produto: rows.dsc_produto,
              dsc_descricao: rows.dsc_descricao,
              cod_produto: rows.cod_produto,
              cod_campanha: rows.cod_campanha,
              cod_regiao: el.cod_regiao,
              departamento: rows.departamento,
              dsc_departamento: rows.dsc_departamento,
              setor: rows.setor,
              dsc_setor: rows.dsc_setor,
              campanhaId: rows.campanhaId,
              vlr_preco_regular: el.vlr_preco_regular,
              vlr_parcela_regular: el.vlr_parcela_regular,
              vlr_preco_clube: el.vlr_preco_clube,
              vlr_parcela_clube: el.vlr_parcela_clube,
              qtd_preco_clube: el.qtd_preco_clube,
              qtd_parcela_clube: el.qtd_parcela_clube
            });
          }
        });
      });
    });
  }

  getProducts(id: number) {
    this.api.getProductDepartamento(id).subscribe(data => {
      const doug = data.results;
      doug.forEach(rows => {
        const region = rows.data.lst_preco_regiao;
        region.forEach(el => {
          if (el.cod_regiao === 5) {
            this.shops.push({
              dsc_produto: rows.dsc_produto,
              dsc_descricao: rows.dsc_descricao,
              cod_produto: rows.cod_produto,
              cod_campanha: rows.cod_campanha,
              cod_regiao: el.cod_regiao,
              departamento: rows.departamento,
              dsc_departamento: rows.dsc_departamento,
              setor: rows.setor,
              dsc_setor: rows.dsc_setor,
              campanhaId: rows.campanhaId,
              vlr_preco_regular: el.vlr_preco_regular,
              vlr_parcela_regular: el.vlr_parcela_regular,
              vlr_preco_clube: el.vlr_preco_clube,
              vlr_parcela_clube: el.vlr_parcela_clube,
              qtd_preco_clube: el.qtd_preco_clube,
              qtd_parcela_clube: el.qtd_parcela_clube
            });
          }
        });
      });
    });
  }

  getProducts2(id: number) {
    this.api.getProductXSetor(11, id).subscribe(data => {
      data.forEach(rows => {
        const region = rows.data.lst_preco_regiao;
        region.forEach(el => {
          if (el.cod_regiao === 5) {
            this.shops.push({
              dsc_produto: rows.dsc_produto,
              dsc_descricao: rows.dsc_descricao,
              cod_produto: rows.cod_produto,
              cod_campanha: rows.cod_campanha,
              cod_regiao: el.cod_regiao,
              departamento: rows.departamento,
              dsc_departamento: rows.dsc_departamento,
              setor: rows.setor,
              dsc_setor: rows.dsc_setor,
              campanhaId: rows.campanhaId,
              vlr_preco_regular: el.vlr_preco_regular,
              vlr_parcela_regular: el.vlr_parcela_regular,
              vlr_preco_clube: el.vlr_preco_clube,
              vlr_parcela_clube: el.vlr_parcela_clube,
              qtd_preco_clube: el.qtd_preco_clube,
              qtd_parcela_clube: el.qtd_parcela_clube
            });
          }
        });
      });
    });
  }
}
