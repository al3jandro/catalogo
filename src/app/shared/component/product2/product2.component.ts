import { Component, OnInit, Input } from "@angular/core";
import { ApiService } from "../../services/api.service";

@Component({
  selector: "app-product2",
  templateUrl: "./product2.component.html",
  styleUrls: ["./product2.component.scss"]
})
export class Product2Component implements OnInit {
  @Input() code: any;
  @Input() regiao: string;
  start: string;
  end: string;
  public shops: any = [];
  imagem: string;
  valor: number;
  items: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.getVigencia(this.code.campanhaId);
    this.imagem = `${this.code.cod_produto}.jpg`;
  }

  getVigencia(campanha: number) {
    this.api.getFindId("Campanhas", campanha).subscribe(row => {
      this.start = row["vigencia_inicio"];
      this.end = row["vigencia_fin"];
    });
  }
}
