import { Observable } from "rxjs";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ApiService } from "../../services/api.service";
@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.scss"]
})
export class ProductComponent implements OnInit {
  @Input() code: any;
  @Input() regiao: string;
  start: string;
  end: string;
  public shops: any = [];
  imagem: string;
  valor: number;
  items: any = [];
  constructor(private api: ApiService) {}

  ngOnInit() {
    this.getVigencia(this.code.campanhaId);
    this.imagem = `${this.code.cod_produto}.jpg`;
  }

  getVigencia(campanha: number) {
    this.api.getFindId("Campanhas", campanha).subscribe(row => {
      this.start = row["vigencia_inicio"];
      this.end = row["vigencia_fin"];
    });
  }
}
