import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class Regiao {
  id: number;
}
export class ApiService {
  public regiao: Regiao;
  private dataSource = new BehaviorSubject(this.regiao);
  data = this.dataSource.asObservable();

  url = "https://marketing.condor.com.br/api";
  token = "R7jGJgWfsd3u0JHhhZXrHjnBBLgKi1XSY1f4UAC6NPom4DP1Y2s0SO3BxwsVZB55";
  constructor(private http: HttpClient) {}

  updatedDataSelection(data: Regiao) {
    this.dataSource.next(data);
  }

  getProductDepartamento(id: number): Observable<any> {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos/findDepartamento?code=${id}&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }


  getProductDepartamentoSearch(search: string): Observable<any> {
    const sql = 'Produtos?[filter][where][departamento]=11';
    const filter = `filter[where][or][0][dsc_produto][like]=%25${search}%25&`;
    const filter2 = `filter[where][or][1][dsc_descricao][like]=%25${search}%25`;
    return this.http
    .get<any[]>( `${this.url}/${ sql }&${ filter }&${ filter2 }&access_token=${this.token}` )
    .pipe(tap(data => data));
  }

  getProduct(code: number) {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos/findProduto?cod_produto=${code}&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }

  getSetor() {
    return this.http
      .get<any[]>(`${this.url}/Produtos/menuSetor?access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  getFindId(table: string, id: number): Observable<any[]> {
    return this.http
      .get<any[]>(`${this.url}/${table}/${id}?access_token=${this.token}`)
      .pipe(tap(data => data));
  }

  getProductXSetor(dep: number, set: number): Observable<any[]> {
    return this.http
      .get<any[]>(
        `${this.url}/Produtos?filter[where][departamento]=${dep}&filter[where][setor]=${set}&access_token=${this.token}`
      )
      .pipe(tap(data => data));
  }
}
