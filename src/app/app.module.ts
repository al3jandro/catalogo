import { BrowserModule } from "@angular/platform-browser";
import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA
} from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { HttpClientModule } from "@angular/common/http";
import localePt from "@angular/common/locales/pt";
import { registerLocaleData } from "@angular/common";
registerLocaleData(localePt);

import { ProductComponent } from "./shared/block/product/product.component";
import { SantaCatarinaComponent } from "./layout/santa-catarina/santa-catarina.component";
import { HomeComponent } from "./layout/home/home.component";
import { Product2Component } from './shared/component/product2/product2.component';
import { FiltroPipe } from './shared/pipes/filtro.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SantaCatarinaComponent,
    ProductComponent,
    Product2Component,
    FiltroPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {}
