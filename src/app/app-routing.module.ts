import { SantaCatarinaComponent } from "./layout/santa-catarina/santa-catarina.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./layout/home/home.component";
import { ApiService } from "./shared/services/api.service";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "parana",
    component: HomeComponent
  },
  {
    path: "santa-catarina",
    component: SantaCatarinaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ApiService]
})
export class AppRoutingModule {}
